using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    private float originalTimeScale;
    public Pacman pacman;
    public AudioSource optionAudioSource;
    public GameObject closePanel;
    public GameObject openPanel;
    public GameObject muteImage;
    public GameObject unmuteImage;

    private void Start()
    {
        originalTimeScale = Time.timeScale;
    }
    public void StartGame()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void StartMenu()
    {
        SceneManager.LoadSceneAsync(0);
    }

    public void PauseGame()
    {
        Time.timeScale = 0; 
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
    }

    public void ExitGame()
    {
        Debug.Log("Exit button clicked."); 
        Application.Quit();
    }


    public void ClosePanel()
    {
        closePanel.SetActive(false);
    }


    public void OpenPanel()
    {
        openPanel.SetActive(true);
    }
    public void ToggleAudio()
    {
        pacman.ToggleAudio();
        optionAudioSource.mute = !optionAudioSource.mute;  
        if (optionAudioSource.mute ==  false)
        {
            muteImage.SetActive(false);
            unmuteImage.SetActive(true);
        }
        else{
            muteImage.SetActive(true);
            unmuteImage.SetActive(false);
        }
    }




}
