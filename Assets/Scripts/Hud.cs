using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class Hud : MonoBehaviour
{
    [SerializeField] Text CoinsText;
    [SerializeField] Text ScoreText;
    [SerializeField] Text EndText;
    [SerializeField] GameObject openPanel;
    private int highScore;
    Pacman pacman;

    void Start()
    {
        GameObject pacmanObject = GameObject.Find("Chomp_Mesh");
        if (pacmanObject != null)
        {
            pacman = pacmanObject.GetComponent<Pacman>();
        }
        highScore = PlayerPrefs.GetInt("HighScore", highScore);
    }

    void Update()
    {
        if (pacman != null)
        {
            if (pacman.score > highScore)
            {
                highScore = pacman.score;
                PlayerPrefs.SetInt("HighScore", highScore);
                PlayerPrefs.Save();
            }
            ScoreText.text = "Score: " + pacman.score.ToString();
            if (pacman.hearts == 0)
            {
                //EndText.color = Color.red;
                EndText.text = "Game Over" + "\n" + "\n"+ "Highest Score: " + highScore + "\n" + "Your Score: " + pacman.score;
                openPanel.SetActive(true);
            }
            else if (pacman.coins == 139)
            {
                EndText.text = "Congrats" + "\n" + "\n" + "\n"+ "Highest Score: " + highScore + "\n" + "Your Score: " + pacman.score;
                pacman.GetComponent<SkinnedMeshRenderer>().enabled = false;
                openPanel.SetActive(true); 
            }

        }
    }

}
